//
//  VirtualObject.swift
//  ARKitplanes
//
//  Created by Katrin on 17.04.2021.
//

import SceneKit

class VirtualObject: SCNReferenceNode {
    
    static let availableOjects: [SCNReferenceNode] = {
        
        guard let modelsURLs = Bundle.main.url(forResource: "art.scnassets", withExtension: nil) else { return [] }
        
        let fileEnumarator = FileManager().enumerator(at: modelsURLs, includingPropertiesForKeys: nil)!
        
        return fileEnumarator.flatMap { element in
            
            let url = element as! URL
            
            guard url.pathExtension == "scn" else { return nil }
            
            return VirtualObject(url: url)
        }
        
    }()
}


